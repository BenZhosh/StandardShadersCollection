Shader "Custom/Noise/Voronoi/Meta Voronoi 2D"
{
    Properties
    {
        [HideInInspector] _MainTex ("Texture", 2D) = "white" {}
        //Voronoi2D
        _VoronoiOffsetSpeed     ("Vornoi Offset Speed",     Vector) = (0, 0, 0, 1)
        _VoronoiAngularSpeed    ("Voronoi Angular Speed",   float)  = 1
        _VoronoiScale           ("Voronoi Scale",           float)  = 1
    }
    SubShader
    {
        Tags {
            "RenderType" = "Transparent"
            "Order" = "Transparent"
        }
        
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite off
        Cull off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            

            #include "UnityCG.cginc"
            #include "Assets/_Client/Utilities/CustomVoronoi2D.hlsl"


            sampler2D _MainTex;
            //Voronoi
            float4 _VoronoiOffsetSpeed;
            float _VoronoiAngularSpeed;
            float _VoronoiScale;
            

            struct MeshData
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct Interpolators
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;

                float3 worldPos : TEXCOORD1;
            };
            

            Interpolators vert (MeshData md)
            {
                Interpolators o;
                
                o.vertex = UnityObjectToClipPos(md.vertex);
                o.uv = md.uv;
                o.color = md.color;

                o.worldPos = mul(unity_ObjectToWorld, md.vertex);
                
                return o;
            }

            float4 frag (Interpolators i) : SV_Target
            {
                float4 c = tex2D(_MainTex, i.uv) * i.color;

                //Voronoi
                float2 v_uv = i.worldPos * (_VoronoiScale / 10);
                v_uv -= _VoronoiOffsetSpeed.xy * (_VoronoiOffsetSpeed.w * _Time.y);
                float angle = _VoronoiAngularSpeed * _Time.y;
                float v_noise = meta_voronoi2D(v_uv, angle);

                c *= v_noise;
                
                return c;
            }
            ENDCG
        }
    }
}
